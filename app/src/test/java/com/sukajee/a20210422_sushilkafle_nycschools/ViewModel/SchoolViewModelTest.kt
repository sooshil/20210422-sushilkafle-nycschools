package com.sukajee.a20210422_sushilkafle_nycschools.ViewModel

import androidx.lifecycle.ViewModelProvider
import com.google.gson.Gson
import com.sukajee.a20210422_sushilkafle_nycschools.model.data.SchoolsListModel
import org.junit.Before
import org.junit.Test
import java.io.FileInputStream
import java.util.*

class SchoolViewModelTest {


    private lateinit var viewModel: SchoolViewModel

    @Before
    fun setUp() {
        viewModel = SchoolViewModel()
    }
    @Test
    fun filterSchool() {
        val response = sampleResponse()
        val schoolList = response.result

        val filteredResult = viewModel.filterSchool("11358")

    }

    private fun sampleResponse(): SchoolsListModel  {
        val testResponseLocation = "src/test/resources/sample_response.json"
        return Gson().fromJson(Scanner(FileInputStream(testResponseLocation)).useDelimiter("\\Z").next(), SchoolsListModel::class.java)
    }
}