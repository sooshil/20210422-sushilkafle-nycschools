package com.sukajee.a20210422_sushilkafle_nycschools.utility;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

import com.sukajee.a20210422_sushilkafle_nycschools.R;

public class MapNavigationHelper {
    public static void navigateToMap(String addressToNavigate, Activity activity) {
        Uri mapUri = Uri.parse(addressToNavigate);

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(mapUri);
        if (activity != null && intent.resolveActivity(activity.getPackageManager()) != null) {
            activity.startActivity(intent);
        } else {
            Toast.makeText(activity, R.string.no_map_data, Toast.LENGTH_SHORT).show();
        }
    }
}
