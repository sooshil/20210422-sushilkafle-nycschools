package com.sukajee.a20210422_sushilkafle_nycschools.utility;

public class Constants {
    public static final String SUCCESS = "SUCCESS";
    public static final String LOADING = "LOADING";
    public static final String FAILURE = "FAILURE";
}
