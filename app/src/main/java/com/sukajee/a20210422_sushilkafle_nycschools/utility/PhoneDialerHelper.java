package com.sukajee.a20210422_sushilkafle_nycschools.utility;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

import com.sukajee.a20210422_sushilkafle_nycschools.R;

public class PhoneDialerHelper {
    public static void dialPhoneNumber(String phoneNumber, Activity activity) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phoneNumber));
        if (activity != null && intent.resolveActivity(activity.getPackageManager()) != null) {
            activity.startActivity(intent);
        } else {
            Toast.makeText(activity, R.string.no_phone_number, Toast.LENGTH_SHORT).show();
        }
    }
}
