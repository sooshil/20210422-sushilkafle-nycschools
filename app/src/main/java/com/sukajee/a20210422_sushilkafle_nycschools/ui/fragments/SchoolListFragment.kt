package com.sukajee.a20210422_sushilkafle_nycschools.ui.fragments

import android.app.Activity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sukajee.a20210422_sushilkafle_nycschools.R
import com.sukajee.a20210422_sushilkafle_nycschools.ViewModel.SchoolViewModel
import com.sukajee.a20210422_sushilkafle_nycschools.databinding.FragmentSchoolListBinding
import com.sukajee.a20210422_sushilkafle_nycschools.model.data.SchoolResult
import com.sukajee.a20210422_sushilkafle_nycschools.model.data.SchoolsListModel
import com.sukajee.a20210422_sushilkafle_nycschools.ui.activity.MainActivity
import com.sukajee.a20210422_sushilkafle_nycschools.ui.adapters.SchoolListAdapter
import com.sukajee.a20210422_sushilkafle_nycschools.utility.Constants
import com.sukajee.a20210422_sushilkafle_nycschools.utility.NetworkState

class SchoolListFragment : Fragment(R.layout.fragment_school_list) {


    private var _binding: FragmentSchoolListBinding? = null
    private val binding get() = _binding!!
    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: SchoolListAdapter
    private lateinit var networkErrorView: ConstraintLayout
    private lateinit var eTextSearchSchools: EditText
    private lateinit var viewModel: SchoolViewModel
    private lateinit var progressBar: ProgressBar
    private var searchString: String = ""
    private lateinit var clearSearchButton: TextView
    private var mAlreadyLoaded: Boolean = false



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentSchoolListBinding.inflate(inflater, container, false)

        binding.apply {
            recyclerView = this.recyclerViewSchoolList
            networkErrorView = this.networkErrorDisplay
            eTextSearchSchools = this.editTextSearchSchools
            progressBar = this.progBar
            clearSearchButton = this.textViewClearSearchText
        }

        if (savedInstanceState == null && !mAlreadyLoaded) {
            mAlreadyLoaded = true;
        }

        if (activity is MainActivity) {
            viewModel = (activity as MainActivity).viewModel

            if (viewModel.schoolsList.value?.result == null) {
                if (NetworkState.isNetworkAvailable(requireContext())) {
                    viewModel.fetchSchoolList()
                    networkErrorView.visibility = View.GONE
                } else {
                    eTextSearchSchools.visibility = View.GONE
                    networkErrorView.visibility = View.VISIBLE

                }
            } else {
                if(savedInstanceState != null) {
                    populateUIOnSuccess((activity as MainActivity).viewModel.filterSchool(searchString))
                }
            }

            viewModel.schoolsList.observe(this, {
                updateUI(it)
            })
        }

        binding.editTextSearchSchools.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                if(eTextSearchSchools.text.toString() != "") {
                    clearSearchButton.visibility = View.VISIBLE
                }
            }
            override fun afterTextChanged(editable: Editable) {
                searchString = editable.toString().trim()
                populateUIOnSuccess((activity as MainActivity).viewModel.filterSchool(searchString))
            }
        })

        clearSearchButton.setOnClickListener {
            eTextSearchSchools.setText("")
            clearSearchButton.visibility = View.GONE
        }

        binding.buttonRetry.setOnClickListener {
            if (NetworkState.isNetworkAvailable(requireContext())) {
                viewModel.fetchSchoolList()
                networkErrorView.visibility = View.GONE
            }
        }
        return binding.root
    }

    private fun updateUI(schoolsListModel: SchoolsListModel) {
        when (schoolsListModel.status) {
            Constants.LOADING -> {
                progressBar.visibility = View.VISIBLE
            }
            Constants.SUCCESS -> {
                eTextSearchSchools.visibility = View.VISIBLE
                progressBar.visibility = View.GONE

                if(searchString.isEmpty()) {
                    adapter = SchoolListAdapter(requireContext(), schoolsListModel.result, itemClickListener)
                    val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context)
                    recyclerView.layoutManager = layoutManager
                    recyclerView.adapter = adapter
                }
            }
            Constants.FAILURE -> {
                progressBar.visibility = View.GONE
                networkErrorView.visibility = View.VISIBLE
                binding.textViewNoNetwork.text = getString(R.string.unable_to_retrieve)
            }
        }
    }

    private fun populateUIOnSuccess(schoolResults: List<SchoolResult>?) {
        if (schoolResults != null && schoolResults.isNotEmpty()) {
            networkErrorView.visibility = View.GONE
            eTextSearchSchools.visibility = View.VISIBLE
        }
        adapter = SchoolListAdapter(context, schoolResults, itemClickListener)
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
    }

    private val itemClickListener = SchoolListAdapter.ItemClickListener { dbn ->
        val fragment: Fragment = SchoolDetailsFragment.newInstance(dbn)
        activity?.supportFragmentManager?.beginTransaction()?.replace(R.id.frameLayout, fragment)?.addToBackStack("schoolListFrag")?.commit()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setActionBar() {
        val activity = activity as MainActivity
        activity.setToolbarTitle("NYC Schools")
        activity.supportActionBar?.setDisplayHomeAsUpEnabled(false)
    }


    override fun onResume() {
        super.onResume()
        setActionBar()
    }
}