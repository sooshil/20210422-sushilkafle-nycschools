package com.sukajee.a20210422_sushilkafle_nycschools.ui.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.sukajee.a20210422_sushilkafle_nycschools.R;
import com.sukajee.a20210422_sushilkafle_nycschools.ViewModel.SchoolViewModel;
import com.sukajee.a20210422_sushilkafle_nycschools.model.data.SATResult;
import com.sukajee.a20210422_sushilkafle_nycschools.model.data.SchoolResult;
import com.sukajee.a20210422_sushilkafle_nycschools.ui.activity.MainActivity;
import com.sukajee.a20210422_sushilkafle_nycschools.utility.MapNavigationHelper;
import com.sukajee.a20210422_sushilkafle_nycschools.utility.NetworkState;
import com.sukajee.a20210422_sushilkafle_nycschools.utility.PhoneDialerHelper;

import java.util.List;
import java.util.Objects;

public class SchoolDetailsFragment extends Fragment {

    public String dbn;
    private ConstraintLayout networkErrorView, schoolDetails, constraintSATDetail;
    private SchoolViewModel viewModel;
    private TextView textViewSchoolName, textViewSchoolAddress, textViewSchoolPhone, textViewSchoolWebsite;
    private TextView textViewSchoolOverview, textViewTestTakers, textViewCriticalReading, textViewWriting, textViewMath;
    private TextView textViewFax, textViewEmail;
    public String addressToNavigate;
    private SchoolResult singleSchool = null;



    //creating instance of fragment to pass dbn to another fragment.
    public static SchoolDetailsFragment newInstance(String dbn) {
        SchoolDetailsFragment fragment = new SchoolDetailsFragment();
        Bundle args = new Bundle();
        args.putSerializable("dbn", dbn);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_school_details, container, false);

        if (getArguments() != null) {
            Bundle bundle = getArguments();
            dbn = bundle.getString("dbn");
        }

        networkErrorView = view.findViewById(R.id.network_error_display);
        textViewSchoolName = view.findViewById(R.id.textViewSchoolName);
        textViewSchoolAddress = view.findViewById(R.id.textViewSchoolAddress);
        textViewSchoolPhone = view.findViewById(R.id.textViewSchoolPhone);
        textViewSchoolWebsite = view.findViewById(R.id.textViewSchoolWebsite);
        textViewFax = view.findViewById(R.id.textViewSchoolFax);
        textViewEmail = view.findViewById(R.id.textViewSchoolEmail);
        textViewSchoolOverview = view.findViewById(R.id.textViewSchoolOverview);
        textViewTestTakers = view.findViewById(R.id.textViewTestTakers);
        textViewMath = view.findViewById(R.id.textViewMath);
        textViewWriting = view.findViewById(R.id.textViewWriting);
        textViewCriticalReading = view.findViewById(R.id.textViewCriticalReading);
        constraintSATDetail = view.findViewById(R.id.constraintSATDetails);
        FloatingActionButton fab_call = view.findViewById(R.id.fab_call);
        FloatingActionButton fab_navigation = view.findViewById(R.id.fab_navigation);
        Button btnRetry = view.findViewById(R.id.buttonRetry);
        schoolDetails = view.findViewById(R.id.schoolDetails);

        Activity activity = getActivity();

        if (activity instanceof MainActivity) {
            ((MainActivity) activity).setToolbarTitle("Schools Details");
            if (((MainActivity) activity).getSupportActionBar() != null) {
                Objects.requireNonNull(((MainActivity) activity).getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
            }
            viewModel = ((MainActivity) activity).viewModel;
            if (Objects.requireNonNull(viewModel.getSATResultList().getValue()).getResult() == null) {
                if(NetworkState.isNetworkAvailable(requireContext())) {
                    viewModel.fetchSATResultList();
                } else {
                    constraintSATDetail.setVisibility(View.GONE);
                    networkErrorView.setVisibility(View.VISIBLE);
                }
            }
            populateSchoolDetail(Objects.requireNonNull(viewModel.getSchoolsList().getValue()).getResult());

            viewModel.getSATResultList().observe(getViewLifecycleOwner(), responseSATData -> {
                if (responseSATData != null) {
                    switch (responseSATData.getStatus()) {
                        case "LOADING":

                        case "SUCCESS":
                            schoolDetails.setVisibility(View.VISIBLE);
                            filterSchoolForSATScore(responseSATData.getResult());
                        case "ERROR":

                    }
                }
            });
        }

        //checks if network available and then fetches data if available else remains as it is.
        btnRetry.setOnClickListener(v -> {
            if(NetworkState.isNetworkAvailable(requireContext())) {
                viewModel.fetchSATResultList();
                networkErrorView.setVisibility(View.GONE);
            }
        });

        fab_navigation.setOnClickListener(v -> {
            if (singleSchool != null) {
                addressToNavigate = "geo:0,0?q=" + singleSchool.getPrimaryAddressLine1().trim() + ", " +
                        singleSchool.getCity().trim() + ", " +
                        singleSchool.getStateCode().trim() + " " +
                        singleSchool.getZip().trim();
                if (activity != null) {
                    MapNavigationHelper.navigateToMap(addressToNavigate, activity);
                }
            }
        });

        fab_call.setOnClickListener(v -> {
            if (singleSchool != null) {
                String phoneNumber = singleSchool.getPhoneNumber().trim();
                if (phoneNumber.length() > 2) {
                    PhoneDialerHelper.dialPhoneNumber(phoneNumber, activity);
                } else {
                    Toast.makeText(getContext(), R.string.no_phone_number, Toast.LENGTH_SHORT).show();
                }
            }
        });
        return view;
    }

    private void filterSchoolForSATScore(List<SATResult> satResults) {
        if (satResults != null && satResults.size() > 0) {
            networkErrorView.setVisibility(View.GONE);
            SATResult currentSchoolResult = satResults.stream().filter(value -> dbn.equals(value.getDbn())).findAny().orElse(null);
            populateUIOnSuccess(currentSchoolResult);
        }
    }

    private void populateUIOnSuccess(SATResult currentSchoolResult) {
        constraintSATDetail.setVisibility(View.VISIBLE);
        networkErrorView.setVisibility(View.GONE);

        if (currentSchoolResult != null) {
            textViewSchoolName.setText(currentSchoolResult.getSchoolName().trim());
            networkErrorView.setVisibility(View.GONE);
            textViewTestTakers.setText(currentSchoolResult.getNumOfSatTestTakers());
            textViewMath.setText(currentSchoolResult.getSatMathAvgScore());
            textViewCriticalReading.setText(currentSchoolResult.getSatCriticalReadingAvgScore());
            textViewWriting.setText(currentSchoolResult.getSatWritingAvgScore());
        }
    }

    private void populateSchoolDetail(List<SchoolResult> result) {

        if (result != null && result.size() > 0) {
            singleSchool = result.stream().filter(value -> dbn.equals(value.getDbn())).findAny().orElse(null);
        }
        if (singleSchool != null) {
            textViewSchoolName.setText(singleSchool.getSchoolName().trim());
            textViewSchoolAddress.setText(String.format(getString(R.string.address_text), singleSchool.getPrimaryAddressLine1().trim(), singleSchool.getCity().trim(), singleSchool.getStateCode().trim(), singleSchool.getZip().trim()));
            if (singleSchool.getPhoneNumber() != null) {
                textViewSchoolPhone.setText(String.format(getString(R.string.phone_text), singleSchool.getPhoneNumber().trim()));
            } else {
                textViewSchoolPhone.setText(R.string.phone_not_available);
            }
            if (singleSchool.getFaxNumber() != null) {
                textViewFax.setText(String.format(getString(R.string.fax_text), singleSchool.getFaxNumber()));
            } else {
                textViewFax.setText(R.string.fax_not_available);
            }
            if (singleSchool.getSchoolEmail() != null) {
                textViewEmail.setText(String.format(getString(R.string.email_text), singleSchool.getSchoolEmail()));
            } else {
                textViewEmail.setText(R.string.email_not_available);
            }
            if (singleSchool.getWebsite() != null) {
                textViewSchoolWebsite.setText(String.format(getString(R.string.website_text), singleSchool.getWebsite().trim()));
            } else {
                textViewSchoolWebsite.setText(R.string.website_not_available);
            }
            if (singleSchool.getOverviewParagraph() != null) {
                textViewSchoolOverview.setText(singleSchool.getOverviewParagraph().trim());
            } else {
                textViewSchoolOverview.setText(R.string.overview_not_available);
            }
        }
    }
}
