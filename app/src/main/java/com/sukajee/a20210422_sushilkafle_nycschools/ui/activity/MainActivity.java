package com.sukajee.a20210422_sushilkafle_nycschools.ui.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.inputmethod.InputMethodManager;

import com.sukajee.a20210422_sushilkafle_nycschools.R;
import com.sukajee.a20210422_sushilkafle_nycschools.ViewModel.SchoolViewModel;
import com.sukajee.a20210422_sushilkafle_nycschools.ui.fragments.SchoolListFragment;
import com.sukajee.a20210422_sushilkafle_nycschools.utility.NetworkState;

public class MainActivity extends AppCompatActivity {


    public SchoolViewModel viewModel;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        viewModel = new ViewModelProvider(this).get(SchoolViewModel.class);

        if (savedInstanceState == null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            SchoolListFragment schoolListFragment = new SchoolListFragment();
            fragmentManager.beginTransaction().add(R.id.frameLayout, schoolListFragment)
                    .commit();
        }

        toolbar.setNavigationOnClickListener(v -> onBackPressed());

    }

    public void setToolbarTitle(String toolbarTitle) {
        toolbar.setTitle(toolbarTitle);
    }


    // Hide keyboard when out of focus
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

}