package com.sukajee.a20210422_sushilkafle_nycschools.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sukajee.a20210422_sushilkafle_nycschools.R;
import com.sukajee.a20210422_sushilkafle_nycschools.model.data.SchoolResult;

import java.util.List;

public class SchoolListAdapter extends RecyclerView.Adapter<SchoolListAdapter.SchoolViewHolder> {
    private final List<SchoolResult> schools;
    private final Context context;
    private final ItemClickListener itemClickListener;

    public SchoolListAdapter(Context context, List<SchoolResult> schools, ItemClickListener itemClickListener) {
        this.context = context;
        this.schools = schools;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public SchoolViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.school_item, parent, false);

        return new SchoolViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SchoolListAdapter.SchoolViewHolder holder, int position) {

        if (schools.get(position).getSchoolName() != null) {
            holder.txtSchoolName.setText(schools.get(position).getSchoolName());
        }
        if (schools.get(position).getPhoneNumber() != null) {
            holder.txtSchoolPhone.setText(String.format(context.getString(R.string.phone_text), schools.get(position).getPhoneNumber().trim()));
        }
        if (schools.get(position).getPrimaryAddressLine1() != null) {
            String address = String.format(context.getString(R.string.address_text), schools.get(position).getPrimaryAddressLine1().trim(), schools.get(position).getCity().trim(), schools.get(position).getStateCode().trim(), schools.get(position).getZip().trim());
            holder.txtSchoolAddress.setText(address);
        }

        holder.btnDetails.setOnClickListener(v -> {
            if (schools.get(position) != null && schools.get(position).getDbn() != null) {
                itemClickListener.onItemClick(schools.get(position).getDbn());
            }
        });
    }

    @Override
    public int getItemCount() {
        if (schools != null) {
            return schools.size();
        } else {
            return 0;
        }
    }

    public interface ItemClickListener {
        void onItemClick(String dbn);
    }

    static class SchoolViewHolder extends RecyclerView.ViewHolder {
        private final TextView txtSchoolName;
        private final TextView txtSchoolPhone;
        private final TextView txtSchoolAddress;
        private final Button btnDetails;

        SchoolViewHolder(View itemView) {
            super(itemView);
            txtSchoolName = itemView.findViewById(R.id.school_name);
            txtSchoolAddress = itemView.findViewById(R.id.school_address);
            txtSchoolPhone = itemView.findViewById(R.id.school_phone);
            btnDetails = itemView.findViewById(R.id.button_details);
        }
    }
}

