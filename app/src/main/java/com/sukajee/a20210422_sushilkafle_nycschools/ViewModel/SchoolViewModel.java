package com.sukajee.a20210422_sushilkafle_nycschools.ViewModel;

import android.content.Context;
import android.util.Log;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.sukajee.a20210422_sushilkafle_nycschools.model.data.SATResult;
import com.sukajee.a20210422_sushilkafle_nycschools.model.data.SATResultListModel;
import com.sukajee.a20210422_sushilkafle_nycschools.model.data.SchoolResult;
import com.sukajee.a20210422_sushilkafle_nycschools.model.data.SchoolsListModel;
import com.sukajee.a20210422_sushilkafle_nycschools.model.network.APIInterface;
import com.sukajee.a20210422_sushilkafle_nycschools.model.network.RetrofitInstance;
import com.sukajee.a20210422_sushilkafle_nycschools.utility.Constants;
import com.sukajee.a20210422_sushilkafle_nycschools.utility.NetworkState;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SchoolViewModel extends ViewModel {

    private static final String TAG = SchoolViewModel.class.getSimpleName();

    MutableLiveData<SchoolsListModel> mutableSchoolList = new MutableLiveData<>();
    MutableLiveData<SATResultListModel> mutableSATResultList = new MutableLiveData<>();


    APIInterface apiInterface = RetrofitInstance.getRetrofitInstance().create(APIInterface.class);

    public LiveData<SchoolsListModel> getSchoolsList() {
        if (mutableSchoolList == null) {
            mutableSchoolList = new MutableLiveData<>();
            fetchSchoolList();
        }
        return mutableSchoolList;
    }


    public LiveData<SATResultListModel> getSATResultList() {
        if (mutableSATResultList == null) {
            mutableSATResultList = new MutableLiveData<>();
            fetchSATResultList();
        }
        return mutableSATResultList;
    }

    public void fetchSchoolList() {
        SchoolsListModel schoolsListModel = new SchoolsListModel();
        Call<List<SchoolResult>> call = apiInterface.getSchools();
        schoolsListModel.setStatus(Constants.LOADING);
        mutableSchoolList.postValue(schoolsListModel);
        call.enqueue(new Callback<List<SchoolResult>>() {
            @Override
            public void onResponse(@NotNull Call<List<SchoolResult>> call, @NotNull Response<List<SchoolResult>> response) {
                Log.d(TAG, "onResponse:" + response.toString());
                schoolsListModel.setStatus(Constants.SUCCESS);
                List<SchoolResult> responseData = response.body();
                schoolsListModel.setResult(responseData);
                mutableSchoolList.postValue(schoolsListModel);

                /*
                calling SAT Details API after School List API call succeeded though this is independent with the previous call
                */
                fetchSATResultList();
            }

            @Override
            public void onFailure(@NotNull Call<List<SchoolResult>> call, @NotNull Throwable t) {
                Log.wtf("Error Logged", t.getMessage());
                schoolsListModel.setStatus(Constants.FAILURE);
                mutableSchoolList.postValue(schoolsListModel);
            }
        });

    }

    public void fetchSATResultList() {
        SATResultListModel satResultListModel = new SATResultListModel();
        Call<List<SATResult>> call = apiInterface.getSatScores();
        satResultListModel.setStatus(Constants.LOADING);
        mutableSATResultList.postValue(satResultListModel);
        call.enqueue(new Callback<List<SATResult>>() {
            @Override
            public void onResponse(@NotNull Call<List<SATResult>> call, @NotNull Response<List<SATResult>> response) {
                Log.d(TAG, "onResponse:" + response.toString());
                satResultListModel.setStatus(Constants.SUCCESS);
                List<SATResult> responseData = response.body();
                satResultListModel.setResult(responseData);
                mutableSATResultList.postValue(satResultListModel);
            }

            @Override
            public void onFailure(@NotNull Call<List<SATResult>> call, @NotNull Throwable t) {
                Log.wtf("Error Logged", t.getMessage());
                satResultListModel.setStatus(Constants.FAILURE);
                mutableSATResultList.postValue(satResultListModel);
            }
        });
    }


    public List<SchoolResult> filterSchool(String text) {
        if (mutableSchoolList != null) {
            List<SchoolResult> filteredSchools;
            filteredSchools = Objects.requireNonNull(mutableSchoolList.getValue())
                    .getResult().stream().filter(value -> value.getSchoolName().toLowerCase().contains(text.toLowerCase()) ||
                            value.getZip().contains(text))
                    .collect(Collectors.toList());
            return filteredSchools;
        } else {
            return null;
        }
    }
}
