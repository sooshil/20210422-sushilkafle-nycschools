package com.sukajee.a20210422_sushilkafle_nycschools.model.data;

import java.util.List;

public class SATResultListModel {

    private String status;
    private List<SATResult> result;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<SATResult> getResult() {
        return result;
    }

    public void setResult(List<SATResult> result) {
        this.result = result;
    }
}

