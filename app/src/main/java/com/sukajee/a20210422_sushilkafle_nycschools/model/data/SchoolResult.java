package com.sukajee.a20210422_sushilkafle_nycschools.model.data;

import com.google.gson.annotations.SerializedName;

public class SchoolResult {
    @SerializedName("dbn")
    private String dbn;

    @SerializedName("school_name")
    private String schoolName;

    @SerializedName("overview_paragraph")
    private String overviewParagraph;

    @SerializedName("phone_number")
    private String phoneNumber;

    @SerializedName("fax_number")
    private String faxNumber;

    @SerializedName("school_email")
    private String schoolEmail;

    @SerializedName("website")
    private String website;

    @SerializedName("primary_address_line_1")
    private String primaryAddressLine1;

    @SerializedName("city")
    private String city;

    @SerializedName("state_code")
    private String stateCode;

    @SerializedName("zip")
    private String zip;

    public String getDbn() {
        return dbn;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public String getOverviewParagraph() {
        return overviewParagraph;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getFaxNumber() {
        return faxNumber;
    }

    public String getSchoolEmail() {
        return schoolEmail;
    }

    public String getWebsite() {
        return website;
    }

    public String getPrimaryAddressLine1() {
        return primaryAddressLine1;
    }

    public String getCity() {
        return city;
    }

    public String getZip() {
        return zip;
    }

    public String getStateCode() {
        return stateCode;
    }
}
