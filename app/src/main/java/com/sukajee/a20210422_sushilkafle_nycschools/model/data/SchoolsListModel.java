package com.sukajee.a20210422_sushilkafle_nycschools.model.data;

import java.util.List;

public class SchoolsListModel {

    private String status;
    private List<SchoolResult> result;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<SchoolResult> getResult() {
        return result;
    }

    public void setResult(List<SchoolResult> result) {
        this.result = result;
    }
}

