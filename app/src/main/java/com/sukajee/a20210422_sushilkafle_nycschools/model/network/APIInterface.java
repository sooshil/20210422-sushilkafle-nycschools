package com.sukajee.a20210422_sushilkafle_nycschools.model.network;

import com.sukajee.a20210422_sushilkafle_nycschools.model.data.SATResult;
import com.sukajee.a20210422_sushilkafle_nycschools.model.data.SchoolResult;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;


public interface APIInterface {

    @GET("/resource/s3k6-pzi2.json/")
    Call<List<SchoolResult>> getSchools();


    @GET("/resource/f9bf-2cp4.json")
    Call<List<SATResult>> getSatScores();
}
